---
title: "Gitlab and Laravel Forge Integrates!"
author: Matt Trask
author_twitter: @matthewtrask
categories: integration
image_title: '/images/blogimages/post-cover-image.jpg'
description: "A deep dive into how to set up a Laravel installation on Gitlab and
Laravel Forge"
---

# Laravel Forge And Gitlab

## Laravel Forge now integrates with Gitlab. Lets see how we can use it!

[Laravel Image]

Laravel Forge, the one click deployment platform from Taylor Otwell has
officially integrated Gitlab with its service so that you can easily connect a
repository on Gitlab to Forge and run a one click deployment. What is great about
Forge is that with that one click deployment, you can also take care of database
migrations, setting new queue and cron jobs, edit your .env file with new 
passwords and API tokens and so much more. So lets take a look at Forge, and how
you can best use it to maximize your time developing and let Forge handle the 
deployment!

### Setting Everything Up

First things first, Forge is a paid service. However, $15 for a sole developer and 
$30 for a team could very well be worth it. But this isn't an article to do the
math (seriously, we are mostly developers, we write code to do math for us!) to
see if its worth it. Once past the sign up page, you will be asked for an API key
from Digital Ocean, AWS Linode or Rackspace. If you are unsure of where to get 
your keys, please visit the respective site. Digital Ocean has amazing documentation
and you will have an API key in no time. Once the key is set, you are presented 
with a panel to spin up a new server off the bat. So lets that do. You will notice
that you are presented with a few options:

* Credentials
* Name (Can and should be changed)
* Server Size (And how much it will cost you a month to run the server)
* Region of the world you want the server spun up at
* PHP Version (Forge already supports 7.1, the latest release)
* Database name
* A few more options: 'Provision as Load Balance', 'Install MariaDB', 'Enable
Digital Ocean weekly backups'

Set the options however you want too. Generally I change the name and database
size. The size can be changed after you create it, so do not worry you are stuck 
on one size or not.

[Laravel Create Server Image]

After the server is provisioned, you will see it listed on the active servers.
Once it is green, you can visit the IP address provided and will see the output 
of the PHP Info Page. Back in Forge, underneath your active servers, you get a list
of events that Forge performed. Good to keep an eye on when you have scheduled
workers or cron jobs to make sure they run! Lets go ahead and set up our Gitlab
repo. You can get there by clicking on the 'Sites' button on the top menu. Once
there you will see an option to either use a Git repository or set up a simple
instance. Choose Git, and go to [gitlab.com](https://gitlab.com) and pick the
repository you want to deploy and get the git remote which is at the top of the
repository. 

[gitlab-forge setup image]

Once all the options are set, click 'Install Repository'. As a note, you should
DEFINITELY make sure 'Install Composer Dependencies'. If you are new to PHP, you
should know that as default, Laravel gitignores the vendor directory which has
the libraries. There are plenty of articles on why you should not commit the
Vendor directory, and if you aren unsure, feel free to message me on Twitter!

After the repository installs, its time to get to work configuring everything so
we have a nice and easy deployment!

### Settings and Fun

Once you set up your repo, you can click on the tab next to Apps, which is 
Environment. In this part of Forge, you can edit the ```.env``` file that comes 
out of the box with Laravel. This is where you can configure passwords to your
database, configure a mail driver to handle mailing uses, and set custom tokens
for services like Twitter, Facebook, TheCatApi, Yelp, BaelorSwift (The premier 
REST API for Taylor Swift <3) and others. Like the Vendor directory, Laravel 
immediately gitignores this and with good reason. Once you commit a ```.env``` 
file with passwords to Git, it can never be replaced. 

#### Queue Workers

Next on the settings, we can set up queue workers. If you have an app that you 
check if users have paid that day and use a worker to take care of it, you 
can set a queue worker to take care of it. You can configure it to try a minimum 
amount of times before it alerts you it failed and how many seconds to rest for.

#### SSL Certificates

Next to last, we can configure an SSL certificate on the server. At this point in
time, it is a no brainer to put an SSL certificate on your application. You can 
do it three ways:

* Create a signing request
* Import an existing one
* Lets Encrpyt (Beta)

I prefer LetsEncrypt, but use which ever one you are most comfortable with. As you
can see with LetsEncrypt, you can tell it what domain names with a comma separated
list. Super easy, as is the theme with Forge, Taylor did a wonderful job making
this as easy and painless as possible.

#### Application Meta Settings

The last setting we have is a Meta setting, in which you can change where the 
public web root is. Laravel defaults to the ```/public``` directory. Lets leave
it be.

## Configuring our Server

We just set up our application. Remember we set values in the ```.env``` file, 
configured our source control for the repository we have here on Gitlab, obtained
a SSL certificate and now it is time to set up the server itself. Out of the box, 
Forge provisions every server with the LEMP Stack. You get:

* Linux - Ubuntu 16.04
* Server - Nginx
* MySQL (and Postgres too!)
* PHP 7.1

The first thing we can configure is the domain and project type of our application.
If you didnt want to do Laravel, you can do a static HTML site, or a Symfony 
application as well. Support is currently being added for more types too, such as
Wordpress, so keep an eye out!

#### SSH Keys

Next up, we add our SSH Keys from our local machine. It is how you can log into 
the instance, so you need to add it if you ever need to ssh into the server. If 
you are unsure, generally on Mac and Linux you can get it with
```cat ~/.ssh/id_rsa.pub```. 

#### Database

We can configure our database next, by setting a Database name, User, Password, 
and create additional users as well. Now, this can configure MySQL or PostgreSQL
so whichever flavor you prefer, you can run with. Laravel uses MySQL out of the 
box, but a simple configuration change in the app will switch it to Postgres.

#### Scheduler and Cron

You can set cron tasks with the Scheduler configuration. If you want your app
to clear all the compiled views, you can set a cron here to use the Laravel
Artisan command to run that and plenty of other commands, either that come with
Laravel or ones you write yourself!

#### Daemon

Almost finished with the configuration, you can set a Daemon to keep parts of the
application always awake. This is key if you are running some async PHP with a 
library like React or Icicle. Keep in mind, this sounds like a lot of work, but
to run all the Bash commands and shell scripts to set this up, what may take an
hour would take a day of work. 

#### Networking

Laravel Forge also lets you configure a network of servers if you need it, and 
configure firewall rules. If you need servers in various regions to keep latency 
at a low, this makes it simple to take care of. We can also set up profiling in the
Monitoring setting. Things like Blackfire.io, which allow you to find
where your code is bogged down, and PaperTrail which allows you to output
production errors for your team to see without sharing SSH access across a team. 

#### Server Meta Settings

Last but not least, we can configure our application to have a maximum size for
image uploads. You can also get a copy of the Laravel Forge SSH key that is
connected to your Gitlab profile. Finally we can update the meta data about the
server. Note the warning, that if you update the ram size or IP Address here, it
does not do anything to the live server. All you are doing is letting Forge know
that these things have changed. Best practice would be to update the server then
update Forge. 

### Live application

To demonstrate everything, I built a simple app that hits an API endpoint at
the TheCatApi.com that was deployed with Forge and lives on Gitlab.com so you can
see the code and see what kind of fun I caused with an api of cat pics and one click
deployment. 

(grrrrrr forge, work for me!)

Once I get forge to work, Ill deploy it. 

### Let us recap

In this article I detailed how to set up a LEMP server with Forge and deploy it 
from our Gitlab repository. Now, one caveat is that I went over every part of Forge, 
but you may or may not need every part of it. Do not worry! Its not a requirement
that every app has a Daemon or needs Queue Workers. However, do have fun! The key
part to take away from this is how easy it is to integrate your Gitlab repository
with Laravel Forge and remove days of work from your plate. Rather then spend time
bogged in a terminal, you can take an hour and have a fully configured server
ready to do. All that leaves you with is more time to work with your team writing
more code and changing the world through code! 

Happy coding!

