# Laravel Forge Gitlab Integration

## What Is Laravel Forge?

    * Saas Platform to spin up Digital Ocean, Linode or AWS Servers 
    * Integrates with Gitlab to do one button push deployments
    * Handles Load Balancing, Queues, and Security from a dead simple Dashboard
    
## Advantages

    * Handles deployments, allowing you and your team to focus on coding 
    * Takes care of database migrations, jobs, SSL Certificates, 
    even server configuration with ease
    * Simple integration with Gitlab, New Relic and other services

## Integrate with Gitlab

    * Commit code to Gitlab like you normally do during the work day
    * In the Forge Dashboard, Authorize the application to integrate with Gitlab
    * In Gitlab, select the branch you want to be the branch you deploy (master,
    deploy). 
    * When ready, press the "Deploy" Button on the dashboard to deploy your code.
    * Should you choose, Forge can automatically run your migrations

    
    
    
    